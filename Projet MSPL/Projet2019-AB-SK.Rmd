---
title: "Rapport de projet de modeles statistiques"
author: "Bekkouch Abderrahmane - Kabad Soufiane"
date: "29 Mars 2019"
output:
  pdf_document: null
  toc: yes
  word_document: default
number_sections: yes
---
  
 ```{r setup, include=FALSE, echo=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


```{r include=FALSE, echo=FALSE}
library(dplyr);
library(magrittr);
library(readr);
library(ggplot2);
library(tidyverse);
library(gridExtra);
library(devtools);
library(wesanderson);
library(tinytex)
```

\clearpage

# Introduction

## **Définition du contexte**
  
- Durant notre recherche d'un sujet interessant à traiter dans le cadre de notre projet de modèles statistiques, nous sommes tombés sur un graphe qui nous a surpris, ce graphe montrait une augumentation criante du nombre de création d'entreprise en France directement après la fin de l'année 2008, année de la grande crise économique mondiale, ce qui nous semblait donc contradictoire, nous avons pris la décision d'étudier donc cet échec de notre intuition, et pour cela on s'est posés les questions suivantes :

## **Choix des questions**

- Quels sont les facteurs qui ont conduit à cette augumentation soudaine du nombre de créations d'entreprise en France ?
  
- Et est-ce-que ce ou ces facteurs persistent au delà de l'année 2009 ?

# Méthodologie

## **Processus de traitement des données**

- Pour étudier les facteurs plausibles ils nous faut de la matière pour comparer, on a donc besoin de données, qu'on a récupérées auprès du site internet de l'INSEE.

- Premièrement, on aura besoin du nombre de création de l'ensemble des entreprises en France par mois et de l'an 2000 au mois de février de 2019, ce choix de la période n'est pas anodin, d'abord il nous offre un contraste plus aigu sur la soudaineté de la croissance qu'on traite et ensuite nous octroie un facilité de traitement car la période est homogène entre toutes les données desquelles on aura besoin pour cette analyse.

-NEECF :Nombre de l'Ensmble des Entreprises Créées en France.

```{r}
NEECF<- read.csv("NEECF.csv", header=TRUE, encoding="UTF-8", sep = ";")
```

- Ensuite nous aurons besoin des données concernant le nombre d'entreprise créées en France sur la meme période \textcolor{red}{mais} en dehors des microentreprises/ autoentrepreneurs, le besoin de ce jeu de données est justifié par notre hypothèse qu'on éclairera un peu plus en bas.

-NEECFHM :Nombre de l'Ensmble des Entreprises Créées en France Hors Microentreprise.

```{r}
NEECFHM<- read.csv("NEECFHM.csv", header=TRUE, encoding="UTF-8", sep = ";")
```

- Et dans le cadre de la meme hypothèse on aura besoin également du nombre de microentreprises créées durant la meme période.

-NMCF :Nombre des Microntreprises Créées en France.

```{r}
NMCF<- read.csv("NMCF.csv", header=TRUE, encoding="UTF-8", sep = ";")
```

- Procédons maintenant à l'extraction de la période sur laquelle ce déroule l'analyse,on n'aura à le faire qu'une seule fois au vu de notre choix expliqué precedemment : 

```{r}
Periode<- unlist(NEECF[1])
```

-Extractions des variables (nombre d'entreprises) pour chaque jeu traités :

```{r}
NEECFv<- as.numeric(unlist(NEECF[2]))
NEECFHMv<- as.numeric(unlist(NEECFHM[2]))
NMCFv<-as.numeric(unlist(NMCF[2]))
```

##Choix de représentation 

-Maintenant que nos données sont filtrées, il nous faut un moyen efficace pour les visualiser et permette une analyse pertinente, notre choix pour cela est un graphique linéaire (courbe), car la comparaison entre données sur une periode fixe y est facile, la première figure (figure 1) surligne la "contradiction" qu'on a explicité précedemment, et initie donc à la reflexion.

-Notre reflexion après avoir vu ce graphique fut de consulter d'autres graphiques concernant les créations d'entreprises, soit par domaine, par département, par type ...
 

```{r, fig.width=15, fig.height= 10}
fig1<-plot(x=Periode,y=NEECFv,type="l",xlab="Période 01/2000 à 02/2019",ylab="Nombre d'entreprise créees",main="Figure 1:Ensemble d'entreprises créées en France entre le 01/2000 et le 02/2019")
lines(x=Periode,y=NEECFv,col="black")
legend(x ="topleft",
c("Ensemble des Entreprises"),
fill = "black"
)
```

- Un second graphique a retenu notre attention et nous a orienté vers un piste probable à tester, le graphique montrait une \textcolor{red}{décroissance} du nombre de création d'entreprises en dehors des microentreprises, chose qui est plus alignée avec notre intuition, et qui nous a permis d'énoncer notre hypothèse, \textcolor{blue}{la croissance est due à l'augumentation du nombre de micro-entreprise créées après 2008}.

-Sur la figure suivant (figure2), le but est de comparer les deux courbes ensemble d'entreprises et ensemble d'entreprise hors micro entreprise, on s'attend bien évidemment à ce que la seconde courbe soit inferieure à la première car compte un type d'entreprise en moins mais on remarque que l'écart entre les deux est très démarqué entre 2008 et 2009.


```{r, fig.width=15, fig.height= 10}
fig2<-plot(x=Periode,y=NEECFv,type="l",xlab="Période 01/2000 à 02/2019",ylab="Nombre d'entreprise créees",main="Figure 2:Comparatif entre nombre de l'ensemble des entreprises créées et l'ensemble des entreprises créées hors microentreprises")
lines(x=Periode,y=NEECFv,col="black")
points(x=Periode,y=NEECFHMv,type="l")
lines(x=Periode,y=NEECFHMv,col="red")
legend(x ="topleft",
c("Ensemble des Entreprises","Ensemble des Entreprises hors micro "),
fill = c("black","red")
)
```

- Notre hypothèse maintenant établie, on a décidé de comparer la courbe du nombres de micro-entreprises créées avec la figure précédente, on remarque que cette courbe ressemble beaucoup à celle de l'ensemble des entreprises créées surtout après l'année 2008, ce qui valide potentiellement notre hypothèse, mais la rigueur n'y est pas, on aura besoin d'un autre graphique pour ça.  

```{r, fig.width=15, fig.height= 10}
fig3<-plot(x=Periode,y=NEECFv,type="l",font.axis=1,xlab="Période 01/2000 à 02/2019",ylab="Nombre d'entreprise créees",main="Figure 3:Comparatif entre nombre de l'ensemble des entreprises créées et l'ensemble des entreprises créées hors microentreprises et microentreprises créées sur la meme période")
lines(x=Periode,y=NEECFv,col="black")
points(x=Periode,y=NEECFHMv,type="l")
lines(x=Periode,y=NEECFHMv,col="red")
points(x=Periode,y=NMCFv,type="l")
lines(x=Periode,y=NMCFv,col="blue")
legend(x ="topleft",
c("Ensemble des Entreprises","Ensemble des Entreprises hors micro ","Microentreprises"),
fill = c("black","red","blue")
)
```

- Un bon indicateur de l'effet réel qu'à la croissance du nombre de microentreprise créées sur la courbe de l'ensemble des entreprises crées serait l'écart existant entre le nombre de micro-entreprises créées et le nombre d'entreprise crées en dehors des micro-entreprises, cet écart montrera l'influence de chaque type sur la courbe de l'ensemble, l'écart sera mesuré par la différence:
\textcolor{blue}{Nombre de micro-entreprises créées} - \textcolor{red}{Nombre d'entreprises créées hors micro-entreprise}.

```{r, fig.width=15, fig.height= 10}
fig4<-plot(x=Periode,y=NEECFv,type="l",font.axis=1,xlab="Période 01/2000 à 02/2019",ylab="Nombre d'entreprise créees",main="Figure 4:Comparatif entre nombre de l'ensemble des entreprises créées et l'écart entre micro-entreprises et autres entreprises")
lines(x=Periode,y=NEECFv,col="black")
points(x=Periode,y=NMCFv-NEECFHMv,type="l")
lines(x=Periode,y=NMCFv-NEECFHMv,col="red")
legend(x ="topleft",
c("Ensemble des Entreprises","Ecart Micro-entreprise et ensemble d'entreprise hors micro-entreprise"),
fill = c("black","red")
)
```

- On remarque beaucoup mieux l'influence des micro-entreprises sur le graphe d'ensemble, les pics représentent les periodes ou l'effet de la création de microentreprises est le plus prononcés et on voit sur le graphique qu'en fin 2008 il y'a un pic et une chute pour un pic sur toute la période 06/2009 à 09/2010. donc notre hypothèse est partiellement validée . la croissance en 2008 est bien due au moins au facteur de création de micro-entreprise, et les pics continuent jusqu'en 2015 preuve que l'effet a persister au moins pour quelque temps, mais le plus interessant est l'existence de pics au bout de l'axe des abscisses vers environ fin 2017 à maintenant, une eventuelle question à se poser. 

-Le graphe en barres ci-dessous montre la difference de l'influence de la création de micro-entreprise entre l'année 2008 (avant crise) et 2009 (post crise), la visualisation entre les deux années note bien qu'il y'a une corrélation et non pas une coincidence.

```{r, fig.width=15, fig.height= 15}
NEECF2008<-sum(NEECFv[122:133])
NEECFHM2008<-sum(NEECFHMv[122:133])
NMCF2008<-sum(NMCFv[122:133])
NEECF2009<-sum(NEECFv[110:121])
NEECFHM2009<-sum(NEECFHMv[110:121])
NMCF2009<-sum(NMCFv[110:121])
fig5<-c(NEECF2008,NEECF2009,NEECFHM2008,NEECFHM2009,NMCF2008,NMCF2009)
fig5p<-barplot(fig5,main="Figure 4 :Nombre d'entreprise créées en France pour les années 2008 et 2009",
ylim=c(0,600000),
ylab="Nombre d'entreprise",
xlab="Type d'entreprise crées (année)",
col=c("darkblue","lightblue","mistyrose", "lightcyan","lavender","cornsilk")
)
legend(x ="top",
c("Ensemble des Entreprises (2008)","Ensemble des Entreprises (2009)","Ensemble des Entreprises hors micro (2008)","Ensemble des Entreprises hors micro (2009)","Microentreprise(2008)","Microentreprise(2009)"),
fill = c("darkblue","lightblue","mistyrose", "lightcyan","lavender","cornsilk")
)
```

#Conclusion et autres questions:

- On remarque donc bien que la croissance du nombre de créations de micro- entreprise est un des facteurs de la croissance soudaine du nombre de créations de l'ensemble des types d'entreprise, on ne peut pas vraiment se prononcer sur le fait qu'il soit l'unique facteur ou pas, une analyse plus poussée serait necessaire pour ça, on peut également voir que cet effet a persisté pour un certain temps mais qu'il s'est estompé vers fin 2015.

-Suite à cette analyse d'autres questions seraient interessantes à traiter, par exemple, quel secteur d'activités et le plus touché par l'augumentation du nombre de micro-entreprise?, au niveau régional et départemental également?, le nombre d'autoentrepreneur a t-il un effet sur le chomage ?